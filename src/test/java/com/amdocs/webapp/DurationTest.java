package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;

public class DurationTest {
    @Test
    public void testDuration() throws Exception {

        String value= new Duration().dur();
        assertTrue("Contains", value.contains("CP-DOF"));
        
    }
}
