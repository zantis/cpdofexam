package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;

public class CustomStringTest {
    @Test
    public void CustomString() throws Exception {

        String value= new CustomString("Value").gstr();
        assertTrue("String equals", "Value".equals(value));
        
    }
}
