package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;

public class MinMaxTest {
    @Test
    public void testMaxNumberFirst() throws Exception {

        int k= new MinMax().maxNumber(12,10);
        assertEquals("First Max", 12, k);
        
    }
    @Test
    public void testMaxNumbersSecond() throws Exception {

        int k= new MinMax().maxNumber(10,12);
        assertEquals("Second Max", 12, k);
        
    }
        @Test
    public void testMaxNumbersEqueals() throws Exception {

        int k= new MinMax().maxNumber(12,12);
        assertEquals("Equals", 12, k);
        
    }
    @Test
    public void testMaxNumbersStringValue() throws Exception {

        String k= new MinMax().bar("value");
        assertEquals("Equals", "value", k);
        
    }
        @Test
    public void testMaxNumbersStringEmpty() throws Exception {

        String k= new MinMax().bar("");
        assertEquals("Equals", "", k);
        
    }
            @Test
    public void testMaxNumbersStringNull() throws Exception {

        String k= new MinMax().bar(null);
        assertEquals("Equals",null, k);
        
    }
}
