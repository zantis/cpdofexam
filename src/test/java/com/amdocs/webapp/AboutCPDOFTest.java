package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;

public class AboutCPDOFTest {
    @Test
    public void testAboutCPDOF() throws Exception {

        String value= new AboutCPDOF().desc();
        assertTrue("Contains", value.contains("CP-DOF"));
        
    }
}
